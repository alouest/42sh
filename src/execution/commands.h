#ifndef COMM
#define COMM

#include <unistd.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <fnmatch.h>
#include <string.h>
#include "42sh.h"
#include "pile.h"
#include "exec.h"
#include "builtins.h"

int sh_comm(struct ast *tree);
int sh_simple(struct ast *tree);
int sh_simple_comm(struct ast *tree, int pos);
int sh_shell(struct ast *tree);

#endif
