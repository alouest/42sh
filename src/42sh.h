/**
 ** \file 42sh.h
 ** \brief contain all common struct and function to the project
 ** \author Yohann BELAIR
 ** \version 0.5
 ** \date 15 november 2016
 */
#ifndef SH_H
#define SH_H
/**
 * \struct set_struct
 * \brief struct containing environment variable
 *
 * Set_struct is a struct containing all envirnoment varaible.
 * Thanks to this struct we can keep with ease and modify
 * env variable
 *
 */
struct set_struct
{
  char *env;
  char *home;
  char *ifs;
  char *lang;
  char *lc_all;
  char *lc_collate;
  char *lc_ctype;
  char *lc_messages;
  char *lineno;
  char *nlspath;
  char *path;
  char *ppid;
  char *ps1;
  char *ps2;
  char *ps4;
  char *pwd;
  char *oldpwd;
  char *dol_at;
  char *dol_inter;
  char *dol_doll;
  char *dol_uid;
  char *shelopts;
  struct token *tk;
  char ast_print;
};
/**
 ** \enum token_type
 ** \brief contain the type of each token
 */
enum token_type
{
  FUNCTION,
  AND_IF,
  Pipe,
  COMMA,
  DLESS,
  DGREAT,
  LESSAND,
  GREATAND,
  LESSGREAT,
  DLESSDASH,
  CLOBBER,
  If,
  Then,
  Else,
  Elif,
  Fi,
  Done,
  Do,
  Case,
  Esac,
  While,
  Until,
  For,
  Lbrace,
  Rbrace,
  Bang,
  In,
  NEWLINE,
  Lpar,
  Rpar,
  OR_IF,
  DSEMI,
  AND,
  GREAT,
  LESS,

  WORD,
  ASSIGNEMENT_WORD,
  NAME,
  IO_NUMBER,
  HEREDOC
};
/**
 * \struct ast
 * \brief general tree, number is the number of children, child the number
 ** of children and name the name of the current node
 */
struct ast
{
  int number;
  struct ast **child;
  char *name;
};
/**
 * \struct token
 * \brief keep each word defined by env varaible IFS
 *
 * Struct token keep word separated by spearator inside env varaiable
 * IFS. Its a linked list with the size of the word and a pointer to
 * that word.
 *
 */
typedef struct token l_token;
struct token
{
  struct token *next;
  struct token *prev;
  unsigned size;
  char *str;
  enum token_type type;
};
/*
enum dec_typ
{
  VAR,
  FUNCTION
};
struct dec
{
  //null terminated pls
  char *str,
  char *val,
  enum dec_type type
};
*/
/* Functions */
/* lexer function */
char *get_stdin(unsigned *rsize);
char *get_file(int fd);
char is_delim(char c);
char op_tokeniser(char *str, unsigned beg, unsigned end, struct set_struct *ss);
int tokenise(struct set_struct *ss, char *str, unsigned size);
void free_token(struct token *tk);
void print_token(struct token *tk);
int proper_tokenise(struct set_struct *ss, char *str, unsigned size);
/* Expansion */
void tilde_expansion(struct set_struct *ss, struct token *tk);
void tilde_plus_expansion(struct set_struct *ss, struct token *tk);
void tilde_minus_expansion(struct set_struct *ss, struct token *tk);
void expansion_manager(struct set_struct *ss, struct token *tk);
void rnd_expansion(struct token *tk);
void rnd_expansion2(struct token *tk);
/* parser function */
int arg_parse(int argc, char *argv[]);
struct ast *parser(l_token **toke);
char build_ast(struct ast *a, unsigned size, char *str);
struct ast *destroy_ast(struct ast *a);
void ast_printer(struct ast *a);
char *to_string(enum token_type type);
struct ast *create_node(char *str);
struct ast *add_son(struct ast *ast, char *str);
struct ast *prepare_son(struct ast *ast);
struct ast *destroy_ast(struct ast *a);
struct ast *add_node(struct ast *ast, struct ast *temp);
/* LL checker */
struct ast *is_list(l_token **token);
struct ast *is_and_or(l_token **token);
struct ast *is_pipeline(l_token **token);
struct ast *is_command(l_token **token);
struct ast *is_simple_command(l_token **token);
struct ast *is_shell_command(l_token **token);
struct ast *is_funcdec(l_token **token);
struct ast *is_redirection(l_token **token);
struct ast *is_prefix(l_token **token);
struct ast *is_element(l_token **token);
struct ast *is_compound_list(l_token **token);
struct ast *is_rule_for(l_token **token);
struct ast *is_rule_while(l_token **token);
struct ast *is_rule_until(l_token **token);
struct ast *is_rule_case(l_token **token);
struct ast *is_rule_if(l_token **token);
struct ast *is_else_clause(l_token **token);
struct ast *is_do_group(l_token **token);
struct ast *is_case_clause(l_token **token);
struct ast *is_case_item(l_token **token);
/* General functions */
void get_all_env(struct set_struct *ss);
#endif
