/**
** \file commands.c
** \brief handle the 42sh commands.
** \authors Benoît MORETTE-BOURNY
** \version 0.8
** \date 22 november 2016
*/

#define _DEFAULT_SOURCE
#include <sys/wait.h>
#include <sys/types.h>
#include "commands.h"

/**
** \fn int sh_comm(struct ast *tree)
** \brief execute 42sh commands.
**
** \return 1 on error, 0 otherwise.
*/
int sh_comm(struct ast *tree)
{
  int ret = 0;
  if (strcmp(tree->child[0]->name, "simple_command") == 0)
    {
      ret = sh_simple(tree->child[0]);
    }
  if (strcmp(tree->child[0]->name, "shell_command") == 0)
    {
      ret = sh_shell(tree->child[0]);
    }
  return ret;
}

/**
** \fn int sh_shell(struct ast *tree)
** \brief execute 42sh shell commands.
**
** \return 1 on error, 0 otherwise.
*/
int sh_shell(struct ast *tree)
{
  int ret = 0;
  if (!strcmp(tree->child[0]->name, "(") || !strcmp(tree->child[0]->name, "{"))
    {
      ret = exec(tree->child[1]);
    }
  else if (strcmp(tree->child[0]->name, "rule_for") == 0)
    ret = exec(tree->child[0]);
  else if (strcmp(tree->child[0]->name, "rule_while") == 0)
    ret = exec(tree->child[0]);
  else if (strcmp(tree->child[0]->name, "rule_until") == 0)
    ret = exec(tree->child[0]);
  else if (strcmp(tree->child[0]->name, "rule_if") == 0)
    ret = exec(tree->child[0]);
   else if (strcmp(tree->child[0]->name, "rule_case") == 0)
     ret = exec(tree->child[0]);
   else
     ret = 1;
  return ret;
}

/**
** \fn int sh_simple(struct ast *tree)
** \brief porcess 42sh simple commands (builtins and executable file).
**
** \return 1 on error, 0 otherwise.
*/
int sh_simple(struct ast *tree)
{
  int ret = 0;
  for (int it = 0; it <= tree->number; it++)
    {
      if (strcmp(tree->child[it]->name, "element") == 0)
        return sh_simple_comm(tree, it);
    }
  return ret;
}

#include <stdio.h>


/**
** \fn static int check_b(struct ast *tree)
** \brief check if the commad is a builtin
**
** \return 1 if the command is a builtin, 0 otherwise
*/
static
int check_b(struct ast *tree)
{
  if (strcmp(tree->name, "cd") == 0)
    return 1;
  if (strcmp(tree->name, "exit") == 0)
    return 1;
  if (strcmp(tree->name, "pwd") == 0)
    return 1;
  return 0;
}

/**
** \fn int exec_b(struct ast *tree, int pos)
** \brief exec the commad if it's a builtin
**
** \return 0 if the builtin succes, 0 otherwise
*/
int exec_b(struct ast *tree, int pos)
{
  if (strcmp(tree->child[pos]->child[0]->name, "pwd") == 0)
    return pwd();
  if(strcmp(tree->child[pos]->child[0]->name, "cd") == 0)
    if (pos + 1 <= tree->number)
      return cd(tree->child[pos + 1]->child[0]->name);
  if (strcmp(tree->child[pos]->child[0]->name, "exit") == 0)
    if (pos + 1 <= tree->number)
      return sh_exit(tree->child[pos + 1]->child[0]->name);
  return 1;
}
  
/**
** \fn int sh_comm(struct ast *tree)
** \brief execute 42sh simple commands (builtins and executable file).
**
** \return 1 on error, 0 otherwise.
*/
int sh_simple_comm(struct ast *tree, int pos)
{
  if (strcmp(tree->child[pos]->child[0]->name, "false") == 0)
    return 1;
  int ret = 0;
  if (check_b(tree->child[pos]->child[0]))
    return exec_b(tree, pos);
  char *argv[tree->number - pos];
  argv[tree->number - pos + 1] = NULL;
  for (int it = pos; it <= tree->number; it++)
      argv[it] = tree->child[it]->child[0]->name;
  pid_t id = fork();
  if (id == -1)
      return 1;
  else if (id == 0)
    {
        execvp(tree->child[pos]->child[0]->name, argv);
    }
  else if (id != 0)
    {
      int ret_val;
      waitpid(id, &ret_val, 0);
      ret = !WIFEXITED(ret_val);
    }
  return ret;
}
