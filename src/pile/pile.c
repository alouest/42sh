/**
** \file pile.c
** \brief pile implementation for the variables   
** \author Benoît MORETTE-BOURNY
** \version 0.5
** \date 16 november 2016
*/
#include "pile.h"

/**
** \fn char *get_var(struct pile *p, char *name)
** \brief search the varaible name on the var_pile.
**
** \return NULL if the variable is not found, the value otherwise
*/
char *get_var(struct pile *p, char *name)
{
  for (struct pile *it = p; it; it = it->next)
    {
      if (strcmp(it->name, name) == 0)
        return it->value;
    }
  return NULL;
}

/**
** \fn struct pile *new()
** \brief create a new variable pile with a sentinel at the bottom of it
**
** \return NULL if an error occur, the empty pile otherwise
*/
struct pile *new()
{
  struct pile *p = malloc(sizeof (struct pile));
  if (p == NULL)
    return NULL;
  p->name = malloc(sizeof (char));
  p->value = malloc(sizeof (char));
  *p->name = 0;
  *p->value = 0;
  p->next = NULL;
  return p;
}


/**
** \fn struct pile *push(struct pile *p, char *name, char *val)
** \brief add the variable name with the value val on the top of pile p
**
** \return NULL if an error occur, the new pile otherwise
*/
struct pile *push(struct pile *p, char *name, char *val)
{
  struct pile *new = malloc(sizeof (struct pile));
  if (new == NULL)
    return NULL;
  new->next = p;
  strcpy(new->value, val);
  strcpy(new->name, name);
  return new;
}

/**
** \f void pop(struct pile *p)
** \brief remove the top variable of the pile p
**
** \return nothing
*/
void pop(struct pile *p)
{
  struct pile *first = p;
  p = p->next;
  free(first);
}


/**
** \f void blast(struct pile *p)
** \brief free the pile
**
** \return nothing
*/
void blast(struct pile *p)
{
  struct pile *first = p;
  while (first)
    {
      struct pile *second = first->next;
      if (first->name && first->name[0] == 0)
        free(first->name);
      if (first->value && first->value[0] == 0)
        free(first->value);
      free(first);
      first = second;
    }
}
