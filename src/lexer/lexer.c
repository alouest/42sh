/**
 ** \file lexer.c
 ** \brief main lexer function
 ** \author Yohann BELAIR
 ** \version 0.1
 ** \date 11 november 2016
 ** 
 ** The lexer get input and transform each of them into token that the parser
 ** can then parse following the schell command line principle
 **
 */
#include <stdlib.h>
#include <stdio.h>
#include <42sh.h>
#include <string.h>
/**
 ** \fn void free_token(struct token *tk)
 ** \brief free properly token structure
 **
 ** \return nothing 
 **
 */
void free_token(struct token *tk)
{
  if (tk)
  {
    free(tk->str);
    free_token(tk->next);
    free(tk);
  }
}
void free_word_token(struct token *tk)
{
  if (tk && tk->type == WORD)
  {
    free_word_token(tk->next);
    free(tk);
  }
}
/**
 ** \fn int tokenise(struct set_struct *ss, char *str)
 ** \brief main function creating token
 **
 ** \return EXIT_SUCCESS - If function is done or have fail 
 **
 */
int tokenise(struct set_struct *ss, char *str, unsigned size)
{
  unsigned beg = 0;
  unsigned end = 0;
  ss->tk = NULL;
  while (beg < size && end < size)
  {
    while (is_delim(str[beg]) && beg < size)
      beg ++;
    end = beg; 
    for (; (!is_delim(str[end]) && end < size) || str[end] == '\n' ;)
      end ++;
    if (op_tokeniser(str, beg, end, ss) == 0)
      return 0;
    beg = end;
  }
  return 1;
}
static void simple_tokenise(struct token *tmp)
{
  char *tmp_c = malloc(sizeof (char) * (tmp->size + 1));
  memcpy(tmp_c, tmp->str, tmp->size);
  tmp->str = tmp_c;
  tmp->str[tmp->size] = 0;
}
static void hard_tokenise(struct token *tk1, struct token *tmp)
{
  struct token *tk_destr = tmp->next;
  unsigned tmp_w_size = (tmp->str - tk1->str) + tmp->size;
  char *tk_tmp_c = malloc(sizeof (char) * (tmp_w_size + 1));
  memcpy(tk_tmp_c, tk1->str, tmp_w_size);
  tk_tmp_c[tmp_w_size] = 0;
  tk1->str = tk_tmp_c;
  tk1->next = tmp->next;
  if (tmp->next)
    tmp->next->prev = tk1;
  free_word_token(tk_destr);
}
static void sep_tokenise(struct token *tmp, char *sep, struct set_struct *ss);
static void no_sep(struct token *tmp, struct token *tk1, char *sep, 
    struct set_struct *ss)
{
  unsigned size;
  hard_tokenise(tk1, tmp);
  printf("%s ", ss->ps2);
  fflush(stdout);
  char *str;
  while (42)
  {
    str = get_stdin(&size);
    unsigned pos;
    for (pos = 0; pos < size; pos++)
      if (str[pos] == *sep)
        break ;
    tk1->str = realloc(tk1->str, strlen(tk1->str) + pos + 1);
    memcpy(tk1->str + strlen(tk1->str) - 1, str, pos + 1);
    tk1->str[strlen(tk1->str) + pos + 1] = 0;
    if (str[pos] == *sep)
    {
      tokenise(ss, str + pos, size);
      sep_tokenise(tk1, sep, ss);
      tmp = tk1;
      return ;
    }
  }
}
static void sep_tokenise(struct token *tmp, char *sep, struct set_struct *ss)
{
  struct token *tk1 = tmp;
  while (tmp->next && strncmp(tmp->next->str, sep, tmp->next->size) != 0)
    tmp = tmp->next;
  if (tmp->next && strncmp(tmp->next->str, sep, tmp->next->size) == 0)
    tmp = tmp->next;
  else
  {
    no_sep(tmp, tk1, sep, ss);
    return ;
  }
  if (tk1 != tmp)
  {
    hard_tokenise(tk1, tmp);
  }
  else
  {
    simple_tokenise(tmp);
  }
}
int proper_tokenise(struct set_struct *ss, char *str, unsigned size)
{
  if (!tokenise(ss, str, size))
    return 0;
  for (struct token *tmp = ss->tk; tmp; tmp = tmp->next)
  {

    if (tmp->type == WORD && strncmp(tmp->str, "'", tmp->size) == 0)
      sep_tokenise(tmp, "'", ss);
    else if (tmp->type == WORD && strncmp(tmp->str, "\"", tmp->size) == 0)
      sep_tokenise(tmp, "\"", ss);
    else
      simple_tokenise(tmp);
  }
  return 1;
}
