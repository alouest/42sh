#ifndef LOOP
#define LOOP

#include <string.h>
#include "42sh.h"
#include "pile.h"
#include "exec.h"

int sh_while(struct ast *tree);
int sh_until(struct ast *tree);
int sh_for(struct ast *tree);

#endif
