/**
** \file parser4.c
** \brief tools for parsing an LL language from the subject
** (more or less a bash grammar)
** \version 1.0
** \date 27 november 2016
*/

#define _XOPEN_SOURCE 600
#include <string.h>
#include <sys/types.h>
#include <pwd.h>
#include <stdlib.h>
#include <string.h>
#include <42sh.h>

/**
** \fn struct ast *is_rule_until(l_token **token)
** \brief parses the pointer to list to token to check if it matched
** the rule_until and returns a malloc_ed ast
** \return NULL if rule is not verified, else returns the corresponding AST
*/

struct ast *is_rule_until(l_token **token)
{
  struct ast *ast = create_node("rule_until");
  struct ast *temp = NULL;
  if ((*token)->type == Until)
  {
    add_son(ast, "until");
    *token = (*token)->next;
  }
  else
    return destroy_ast(ast);
  if ((temp = is_compound_list(token)))
    add_node(ast, temp);
  else
    return destroy_ast(ast);
  if ((temp = is_do_group(token)))
    add_node(ast, temp);
  else
    return destroy_ast(ast);
  return ast;
}

static struct ast *is_rule_case2(l_token **token, struct ast *ast)
{
  struct token *save = *token;
  struct ast *temp = NULL;
  if ((*token)->type == In)
  {
    add_son(ast, "in");
    *token = (*token)->next;
  }
  else
    return destroy_ast(ast);
  while ((*token)->type == NEWLINE)
    *token = (*token)->next;
  save = *token;
  if ((temp = is_case_clause(token)))
    add_node(ast, temp);
  else
    *token = save;
  if ((*token)->type == Esac)
  {
    add_son(ast, "esac");
    *token = (*token)->next;
  }
  else
    return destroy_ast(ast);
  return ast;
}

/**
** \fn struct ast *is_rule_case(l_token **token)
** \brief parses the pointer to list to token to check if it matched
** the rule_case and returns a malloc_ed ast
** \return NULL if rule is not verified, else returns the corresponding AST
*/

struct ast *is_rule_case(l_token **token)
{
  struct ast *ast = create_node("rule_case");

  if ((*token)->type == Case)
  {
    add_son(ast, "case");
    *token = (*token)->next;
  }
  else
    return destroy_ast(ast);
  if ((*token)->type == WORD)
  {
    add_son(ast, (*token)->str);
    *token = (*token)->next;
  }
  else
    return destroy_ast(ast);
  while ((*token)->type == NEWLINE)
    *token = (*token)->next;
  return is_rule_case2(token, ast);
}

static struct ast *is_rule_if2(l_token **token, struct ast *ast)
{
  struct token *save = *token;
  struct ast *temp = NULL;
  if ((temp = is_compound_list(token)))
    add_node(ast, temp);
  else
    return destroy_ast(ast);
  save = *token;
  if ((temp = is_else_clause(token)))
    add_node(ast, temp);
  else
    *token = save;
  if ((*token)->type == Fi)
  {
    add_son(ast, "fi");
    *token = (*token)->next;
  }
  else
    return destroy_ast(ast);
  return ast;
}

/**
** \fn struct ast *is_rule_if(l_token **token)
** \brief parses the pointer to list to token to check if it matched
** the rule_if and returns a malloc_ed ast
** \return NULL if rule is not verified, else returns the corresponding AST
*/

struct ast *is_rule_if(l_token **token)
{
  struct ast *ast = create_node("rule_if");
  struct ast *temp = NULL;
  if ((*token)->type == If)
  {
    add_son(ast, "if");
    *token = (*token)->next;
  }
  else
    return destroy_ast(ast);
  if ((temp = is_compound_list(token)))
    add_node(ast, temp);
  else
    return destroy_ast(ast);
  if ((*token)->type == Then)
  {
    add_son(ast, "then");
    *token = (*token)->next;
  }
  else
    return destroy_ast(ast);
  return is_rule_if2(token, ast);
}

static struct ast *is_else_clause2(l_token **token, struct ast *ast)
{
  struct token *save = *token;
  struct ast *temp = NULL;
  if ((*token)->type == Then)
  {
    add_son(ast, "then");
    *token = (*token)->next;
  }
  else
    return destroy_ast(ast);
  if ((temp = is_compound_list(token)))
    add_node(ast, temp);
  else
    return destroy_ast(ast);
  save = *token;
  if ((temp = is_case_clause(token)))
    return add_node(ast, temp);
  else
    *token = save;
  return ast;
}

/**
** \fn struct ast *is_rule_clause(l_token **token)
** \brief parses the pointer to list to token to check if it matched
** the rule_clause and returns a malloc_ed ast
** \return NULL if rule is not verified, else returns the corresponding AST
*/

struct ast *is_else_clause(l_token **token) //
{
  struct ast *ast = create_node("rule_else");
  struct ast *temp = NULL;
  int is_else = 0;
  if ((*token)->type == Elif || (*token)->type == Else)
  {
    if ((*token)->type == Else)
      is_else = 1;
    add_son(ast, to_string((*token)->type));
    *token = (*token)->next;
  }
  else
    return destroy_ast(ast);
  if ((temp = is_compound_list(token)))
    add_node(ast, temp);
  else
    return destroy_ast(ast);
  if (is_else)
    return ast;
  return is_else_clause2(token, ast);
}

/**
** \fn struct ast *is_do_group(l_token **token)
** \brief parses the pointer to list to token to check if it matched
** the do_group and returns a malloc_ed ast
** \return NULL if rule is not verified, else returns the corresponding AST
*/

struct ast *is_do_group(l_token **token)
{
  struct ast *ast = create_node("do_group");
  struct ast *temp = NULL;
  if (*token == NULL)
  {
    return destroy_ast(ast);
  }
  if ((*token)->type == Do)
  {
    add_son(ast, "do");
    *token = (*token)->next;
  }
  else
    return destroy_ast(ast);
  if ((temp = is_compound_list(token)))
    add_node(ast, temp);
  else
    return destroy_ast(ast);
  if ((*token)->type == Done)
  {
    add_son(ast, "done");
    *token = (*token)->next;
  }
  else
    return destroy_ast(ast);
  return ast;
}
