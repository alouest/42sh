/**
 ** \file 42sh.c
 ** \brief main 42sh function
 ** \version 0.5
 ** \date 18 november 2016
 */
#define _XOPEN_SOURCE
#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>
#include <signal.h>
#include <42sh.h>
#include <exec.h>
/**
 ** \fn static int interc(struct set_struct *ss)
 ** \brief interactive mode
 **
 */
static int interc(struct set_struct *ss)
{
  if (isatty(0))
    if (ss->ps1)
    {
      printf("%s", ss->ps1);
      fflush(stdout);
    }
  unsigned size;
  char *str = get_stdin(&size);
  if (!str)
    return 1;
  ss->tk = NULL;
  proper_tokenise(ss, str, size);
  struct token *tmp_tk = ss->tk;
  while (tmp_tk)
  {
    expansion_manager(ss, tmp_tk);
    tmp_tk = tmp_tk->next;
  }
  tmp_tk = ss->tk;
  struct ast *a;
  int ret = 0;
  if ((ss->tk) && (a = parser(&ss->tk)))
    ret += exec(a);
  free(str);
  destroy_ast(a);
  free_token(tmp_tk);
  ss->tk = NULL;
  return ret;
}
int main(int argc, char **argv)
{
  int ret = arg_parse(argc, argv);
  if (!ret)
    return 0;
  if (ret == -1)
  {
    struct set_struct *ss = malloc(sizeof (struct set_struct));
    ss->tk = NULL;
    get_all_env(ss);
    while (42)
      interc(ss);
    return 0;
  }
  return 1;
}
