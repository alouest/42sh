/**
** \file pile.h
** \brief Handle the stackt structure
** \author Benoît MORETTE-BOURNY
** \version 0.5
** \date 18 novembre 2016
*/ 
#ifndef PILE
#define PILE

#include <stdlib.h>
#include <stddef.h>
#include <string.h>
/**
** \struct pile
** \brief struct containing the stack
**
*/
struct pile
{
  char *name;
  char *value;
  struct pile *next;
};

char *get_var(struct pile *p, char *name);
struct pile *new();
struct pile *push(struct pile *p, char *name, char *val);
void pop(struct pile *p);
void blast(struct pile *p);

#endif
