/**
** \file parser.c
** \brief tools for parsing an LL language from the subject
** (more or less a bash grammar)
** \author Tristan RUTER-NAON
** \version 1.0
** \date 27 november 2016
*/

#define _XOPEN_SOURCE 600
#include <string.h>
#include <sys/types.h>
#include <pwd.h>
#include <stdlib.h>
#include <string.h>
#include <42sh.h>

/**
** \fn struct ast *parser(l_token **token)
** \brief parses the pointer to list of token and returns a malloc'ed  AST,
** moves the pointer of pointer to token at the end of the list
** \return NULL if grammar is not correct, else returns the corresponding AST
*/

struct ast *parser(l_token **token)
{
  struct ast *ast = create_node("input");
  l_token **save = token;
  if (save == NULL || *save == NULL)
    return add_son(ast, "EOF");
  else if ((*save)->type == NEWLINE)
    return add_son(ast, "\n");
  add_node(ast, is_list(save));
  if (ast->child[ast->number] == NULL)
    return destroy_ast(ast);
  if ((*save) == NULL || save == NULL)
    return add_son(ast, "EOF");
  else if ((*save)->type == NEWLINE)
    return add_son(ast, "\n");
  return destroy_ast(ast);
}

/**
** \fn struct ast *is_list(l_token **token)
** \brief parses the pointer to list of token to check if it matches
** the list rule and returns a malloc'ed AST
** \return NULL if rule is not verified, else returns the corresponding AST
*/

struct ast *is_list(l_token **token)
{
  struct ast *ast = create_node("list");
  struct ast *temp = NULL;
  l_token *save = *token;
  add_node(ast, is_and_or(token));
  if (!ast->child[ast->number])
    return destroy_ast(ast);
  while ((*token) != NULL &&
          ((*token)->type == COMMA || (*token)->type == AND))
  {
    add_son(ast, to_string((*token)->type));
    *token = (*token)->next;
    save = *token;
    if ((temp = is_and_or(token)))
      add_node(ast, temp);
    else
    {
      *token = save;
      break;
    }
  }
  return ast;
}

/**
** \fn struct ast *is_and_or(l_token **token)
** \brief parses the pointer to list of token to check if it matches
** the and_or rule and returns a malloc'ed AST
** \return NULL if rule is not verified, else returns the corresponding AST
*/

struct ast *is_and_or(l_token **token)
{
  struct ast *ast = create_node("and_or");
  struct ast *temp = NULL;
  add_node(ast, is_pipeline(token));
  if (!ast->child[ast->number])
    return destroy_ast(ast);
  while ((*token != NULL) &&
          ((*token)->type == AND_IF || (*token)->type == OR_IF))
  {
    add_son(ast, to_string((*token)->type));
    *token = (*token)->next;
    while (*token && (*token)->type == NEWLINE)
      *token = (*token)->next;
    if ((temp = is_pipeline(token)))
      add_node(ast, temp);
    else
      return destroy_ast(ast);
  }
  return ast;
}

/**
** \fn struct ast *is_pipeline(l_token **token)
** \brief parses the pointer to list of token to check if it matches
** the pipeline rule and returns a malloc'ed AST
** \return NULL if rule is not verified, else returns the corresponding AST
*/

struct ast *is_pipeline(l_token **token)
{
  if (*token == NULL)
    return NULL;
  struct ast *ast = create_node("pipeline");
  struct ast *temp = NULL;
  if ((*token)->type == Bang)
  {
    add_son(ast, "!");
    *token = (*token)->next;
  }
  add_node(ast, is_command(token));
  if (!ast->child[ast->number])
    return destroy_ast(ast);
  while ((*token) != NULL && (*token)->type == Pipe)
  {
    add_son(ast, to_string((*token)->type));
    *token = (*token)->next;
    while ((*token) != NULL && (*token)->type == NEWLINE)
      *token = (*token)->next;
    if ((temp = is_command(token)))
      add_node(ast, temp);
    else
      return destroy_ast(ast);
  }
  return ast;
}

/**
** \fn struct ast *is_command(l_token **token)
** \brief parses the pointer to list of token to check if it matches
** the command rule and returns a malloc'ed AST
** \return NULL if rule is not verified, else returns the corresponding AST
*/

struct ast *is_command(l_token **token)
{
  struct token *save = *token;
  struct ast *ast = create_node("command");
  struct ast *temp = NULL;
  add_node(ast, is_simple_command(token));
  if (ast->child[ast->number])
    return ast;
  *token = save;
  ast->child[ast->number] = is_shell_command(token);
  if (!ast->child[ast->number])
  {
    *token = save;
    ast->child[ast->number] = is_funcdec(token);
    if (!ast->child[ast->number])
      return destroy_ast(ast);
  }
  while((temp = is_redirection(token)))
    add_node(ast, temp);
  return ast;
}
