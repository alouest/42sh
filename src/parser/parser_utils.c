/**
** \file parser_utils.c
** \brief utils for parser
** \author Tristan RUTER-NAON
** \version 1.0
** \date 27 november 2016
*/

#define _XOPEN_SOURCE 200809L
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <42sh.h>
#include <fcntl.h>
#include <unistd.h>

/**
** \fn struct ast *destroy_ast(struct ast *a)
** \brief destroy ast
** \return NULL
**
*/

struct ast *destroy_ast(struct ast *a)
{
  if (a)
  {
    for (int i = 0; i <= a->number; i++)
      destroy_ast(a->child[i]);
    free(a->child);
    free(a->name);
    free(a);
  }
  return NULL;
}

/**
** \fn struct ast *add_node(struct ast *ast, struct ast *temp)
** \brief increment the number of child, realloc list of child
** and then add temp at the end of the list
** \return ast
**
*/

struct ast *add_node(struct ast *ast, struct ast *temp)
{
  ast->number++;
  ast->child = realloc(ast->child, (ast->number + 1) * sizeof (struct ast*));
  ast->child[ast->number] = temp;
  return ast;
}

/**
** \fn struct ast *prepare_son(struct ast *ast)
** \brief increment the number of child, realloc list of child
** \return ast
**
*/

struct ast *prepare_son(struct ast *ast)
{
  ast->number++;
  ast->child = realloc(ast->child, (ast->number + 1) * sizeof (struct ast*));
  return ast;
}

/**
** \fn struct ast *add_son(struct ast *ast, char *str)
** \brief increment the number of child, realloc list of child,
** create a new child of str name and add it at the end of the list
** \return ast
**
*/

struct ast *add_son(struct ast *ast, char *str)
{
  prepare_son(ast);
  ast->child[ast->number] = create_node(str);
  return ast;
}

/**
** \fn struct ast *create_node(char *str)
** \brief create a new node for the ast, if str is NULL then name while be
** NULL, else, the value will be copied in a new malloc'ed zone and given 
** to ast->name
** \return ast
**
*/

struct ast *create_node(char *str)
{
  struct ast *a = malloc(sizeof (struct ast));
  a->number = -1;
  if (!str)
    a->name = NULL;
  else
    a->name = strdup(str);
  a->child = NULL;
  return a;
}
