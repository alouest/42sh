#ifndef EXEC
#define EXEC

#include "42sh.h"
#include "pile.h"
#include "operator.h"
#include "commands.h"
#include "condition.h"
#include "loop.h"

struct env
{
  struct pile *var;
  int pipe[2];
};

/**
** \struct pile
** \brief its a global stack
**
*/
struct env *environement;

int init_exec(struct ast *tree);
int exec (struct ast *tree);

#endif
