/**
** \file builtins.c
** \brief handle the 42sh basic builtins.
** \authors Benoît MORETTE-BOURNY
** \version 1
** \date 22 november 2016
*/

#define _POSIX_C_SOURCE 200112L

#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>

/**
** \fn int pwd()
** \brief print the PWD.
**
** \return 1 on error, 0 otherwise.
*/
int pwd()
{
  char *pwd = getenv("PWD");
  printf("%s\n", pwd);
  return 0;
}

/**
** \fn int cd(char *path)
** \brief change the working directory.
**
** \return 1 on error, 0 otherwise.
*/
int cd(char *path)
{
  char *pwd = getcwd(NULL, 256);
  char *realpath = malloc (sizeof (char) * 256);
  realpath = strcat(pwd, "/");
  realpath = strcat(realpath, path);
  fflush(stdout);
  chdir(realpath);
  setenv("PWD", realpath, 1);
  return 0;
}

/**
** \fn void exit(char *arg)
** \brief exit the shell.
**
** \return nothing.
*/
void sh_exit(char *arg)
{
  int ret = atoi(arg);
  exit(ret);
}
