import subprocess
import argparse
class my_color:
    HEADER = '\033[95m'
    BLUE = '\033[94m'
    GREEN = '\033[92m'
    YELLOW = '\033[93m'
    RED = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    BLINK_RED = '\033[31;5m'
    BLINK_GREEN = '\033[32;5m'
    UNDERLINE = '\033[4m'
final = 0
parser = argparse.ArgumentParser()
parser.add_argument('-s', '--sanity', help='execute the test suite with sanity checks enabled', action='store_true')
args = parser.parse_args()
if args.sanity :
    print (my_color.BLUE + "Test suit 42sh with sanity check" + my_color.ENDC)
    print (my_color.YELLOW + "Test if pipe in 42sh work like pipe in bash" + my_color.ENDC)
    print ("-------------------------------------------")
    print (my_color.BLUE + "         Testing ls | wc -l" + my_color.ENDC)
    ret = subprocess.run(["valgrind --leak-check=full --error-exitcode=42 ./../42sh -c ls | wc -l"], shell=True, stdout=subprocess.PIPE)
    if ret.returncode == 42:
        final = final + 1
        print (my_color.BLINK_RED)
        print ("-------------------------------------------")
        print ("                   FAIL")
        print ("-------------------------------------------")
        print (my_color.ENDC)
    else:
        print (my_color.BLINK_GREEN)
        print ("-------------------------------------------")
        print ("                    OK")
        print ("-------------------------------------------")
        print (my_color.ENDC)
    
    
    print (my_color.BLUE + "         Testing ls" + my_color.ENDC)
    ret = subprocess.run(["valgrind --leak-check=full --error-exitcode=42 ./../42sh -c ls"], shell=True, stdout=subprocess.PIPE)
    bret = subprocess.run(["ls"], shell=True, stdout=subprocess.PIPE)
    if ret.stdout != bret.stdout and ret.returncode != bret.returncode:
        print (my_color.BLINK_RED + "         KO" + my_color.ENDC)
        final = final + 1
    else:
        print (my_color.BLINK_GREEN + "         OK" + my_color.ENDC)
    print ("-------------------------------------------")
    print (my_color.BLUE + "         Testing tree / | grep png" + my_color.ENDC)
    ret = subprocess.run(["valgrind --leak-check=full --error-exitcode=42 ./../42sh -c \"tree / grep png\""], shell=True, stdout=subprocess.PIPE)
    if ret.returncode == 42:
        final = final + 1
        print (my_color.BLINK_RED)
        print ("-------------------------------------------")
        print ("                   FAIL")
        print ("-------------------------------------------")
        print (my_color.ENDC)
    else:
        print (my_color.BLINK_GREEN)
        print ("-------------------------------------------")
        print ("                    OK")
        print ("-------------------------------------------")
        print (my_color.ENDC)
    
    
    if final > 0:
        print (my_color.BLINK_RED + "             Test Failed" + my_color.ENDC)
    else:
        print (my_color.BLINK_GREEN + "            Test Passed" + my_color.ENDC)
else :
    print (my_color.BLUE + "Test suit 42sh" + my_color.ENDC)
    print (my_color.YELLOW + "Test if pipe in 42sh work like pipe in bash" + my_color.ENDC)
    print ("-------------------------------------------")
    print (my_color.BLUE + "         Testing ls | wc -l" + my_color.ENDC)
    ret = subprocess.run(["./../42sh -c ls | wc -l"], shell=True, stdout=subprocess.PIPE)
    bret = subprocess.run(["ls | wc -l"], shell=True, stdout=subprocess.PIPE)
    if ret.stdout != bret.stdout and ret.returncode != bret.returncode:
        print (my_color.BLINK_RED + "         KO" + my_color.ENDC)
        final = final + 1
    else:
        print (my_color.BLINK_GREEN + "         OK" + my_color.ENDC)
    print ("-------------------------------------------")
    print ("-------------------------------------------")
    print (my_color.BLUE + "         Testing ls" + my_color.ENDC)
    ret = subprocess.run(["./../42sh -c ls"], shell=True, stdout=subprocess.PIPE)
    bret = subprocess.run(["ls"], shell=True, stdout=subprocess.PIPE)
    if ret.stdout != bret.stdout and ret.returncode != bret.returncode:
        print (my_color.BLINK_RED + "         KO" + my_color.ENDC)
        final = final + 1
    else:
        print (my_color.BLINK_GREEN + "         OK" + my_color.ENDC)
    print ("-------------------------------------------")
    print ("-------------------------------------------")
    print (my_color.BLUE + "         Testing tree / | grep png" + my_color.ENDC)
    ret = subprocess.run(["./../42sh -c \"tree / grep png\""], shell=True, stdout=subprocess.PIPE)
    bret = subprocess.run(["tree / grep png"], shell=True, stdout=subprocess.PIPE)
    if ret.stdout != bret.stdout and ret.returncode != bret.returncode:
        print (my_color.BLINK_RED + "         KO" + my_color.ENDC)
        final = final + 1
    else:
        print (my_color.BLINK_GREEN + "         OK" + my_color.ENDC)
    print ("-------------------------------------------")
    if final > 0:
        print (my_color.BLINK_RED + "             Test Failed" + my_color.ENDC)
    else:
        print (my_color.BLINK_GREEN + "            Test Passed" + my_color.ENDC)

