/**
 ** \file utils.c
 ** \brief lexer functions
 ** \author Yohann BELAIR
 ** \version 0.1
 ** \date 11 november 2016
 **
 ** The lexer get input and transform each of them into token that the parser
 ** can then parse following the schell command line principle
 **
 */
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <42sh.h>
/**
 ** \fn char *get_stdin(unsigned *rsize)
 ** \brief read all stdin
 **
 ** \return allocate a string, we must remember to free it
 **
 */
char *get_stdin(unsigned *rsize)
{
  unsigned size = 0;
  char *buf = NULL;
  unsigned offset = 0;
  *rsize = 0;
  do
  {
    size ++;
    buf = realloc(buf, size);
    if (!buf)
    {
      fprintf(stderr, "GET_LINE ERROR : BUFFER CANNOT BE ALLOCATED\n");
      return NULL;
    }
    *rsize += offset;
  }
  while ((offset = read(0, buf + size - 1, 1)) != 0 && buf[size - 1] != '\n');
  buf = realloc(buf, size + 1);
  buf[size]  = 0;
  *rsize += offset;
  return buf;
}
/**
 ** \fn char *get_file(int fd)
 ** \brief read all content of file passed as a file descriptor
 **
 ** \return allocate a string, we must remember to free it
 **
 */
char *get_file(int fd)
{
  unsigned size = 0;
  char *buf = NULL;
  unsigned offset = 0;
  do
  {
    size += 256;
    buf = realloc(buf, size);
    if (!buf)
    {
      fprintf(stderr, "GET_LINE ERROR : BUFFER CANNOT BE ALLOCATED\n");
      return NULL;
    }
  }
  while ((offset = read(0, buf, 256)) != 0);
  close(fd);
  return buf;
}

/**
 ** \fn char is_delim(char c)
 ** \brief test if current character c is a delimiter
 **
 ** \return return 1 if c is a delimiter, 0 otherwise
 **
 */
char is_delim(char c)
{
  char list[] =  " \t\n";
  for (unsigned i = 0; list[i] != 0; i++)
    if (list[i] == c)
      return 1;
  return 0;
}
/**
 ** \fn enum token_type is_name(struct token *tk)
 ** \brief check is token is a name
 **
 ** \return return 1 if tk is a name, 0 otherwise
 **
 */
static enum token_type is_name(struct token *tk)
{
  if (tk->str[0] >= '0' && tk->str[0] <= '9')
    return WORD;
  for (unsigned i = 0; i < tk->size; i++)
    if (!((tk->str[i] >= '0' && tk->str[i] <= '9') || (tk->str[i] == '_')
        || (tk->str[i] >= 'a' && tk->str[i] <= 'z')
        || (tk->str[i] >= 'A' && tk->str[i] <= 'Z')))
      return WORD;
  return NAME;
}
/**
 ** \fn char contain_equal(struct token *tk)
 ** \brief test if token contain equal symbol inside but not a the begining
 **
 ** \return return 1 if token is a valid name, 0 otherwise
 **
 */
static char contain_equal(struct token *tk)
{
  if (tk->str[0] == '=')
    return 0;
  for (unsigned i = 1; i < tk->size; i++)
    if (tk->str[i] == '=')
      return 1;
  return 0;
}
/**
 ** \fn char is_number(struct token *tk)
 ** \brief test if token string is a number
 **
 ** \return return 1 if token is a valid number, 0 otherwise
 **
 */
static char is_number(struct token *tk)
{
  for (unsigned i = 1; i < tk->size; i++)
    if (!(tk->str[i] >= '0' && tk->str[i] <= '9'))
      return 0;
  return 1;
}
/**
 ** \fn enum token_type create_tok_type(struct token *tk)
 ** \brief define which type is a token
 **
 ** \return return enum defining type of token
 **
 */
static enum token_type create_tok_type(struct token *tk)
{
  if (tk->prev && (tk->prev->type == LESSGREAT || tk->prev->type ==  DGREAT
      || tk->prev->type == GREATAND || tk->prev->type == LESSAND 
      || tk->prev->type == CLOBBER) 
      && (strncmp(tk->prev->str, "<", tk->prev->size) == 0
      || strncmp(tk->prev->str, ">", tk->prev->size) == 0))
    return WORD;

  if (tk->prev && (tk->prev->type == DLESS || tk->prev->type ==  DLESSDASH))
    return HEREDOC;
  const char *c_op[] = { "function", "&&", "|", ";", "<<", ">>", "<&", ">&", 
    "<>", "<<-", ">|", "if", "then", "else", "elif", "fi", "done", "do", 
    "case", "esac", "while", "until", "for", "{", "}", "!", "in", "\n", "(", 
    ")", "||", ";;", "&", ">", "<"};
  for (unsigned i = 0; i < sizeof (c_op) / sizeof (*c_op); i++)
  {
    unsigned len = strlen(c_op[i]);
    if (tk->size == len && strncmp(c_op[i], tk->str, tk->size) == 0)
    {
      if (tk->prev && is_number(tk->prev) 
          && ((i >= 3 && i <= 9) || strncmp(">", tk->str, tk->size) == 0))
        tk->prev->type = IO_NUMBER;
      return i;
    }
  }
  if (contain_equal(tk))
    return ASSIGNEMENT_WORD;
  if (tk->prev && tk->prev->type == For)
    return is_name(tk);
  return WORD;
}
/**
 ** \fn void creat_tok(struct token *tk, char *ptr, unsigned size)
 ** \brief read a token list for testing purpose
 **
 ** \return nothing
 **
 */
static char create_tok(struct set_struct *ss, char *ptr, unsigned size)
{
  struct token *tk;
  struct token *res = malloc( sizeof (struct token));
  if (!res)
    return 0;
  else
  {
    if (!ss->tk)
    {
      ss->tk = res;
      tk = ss->tk;
      tk->prev = NULL;
    }
    else
    {
      tk = ss->tk;
      while (tk && tk->next)
        tk = tk->next;
      tk->next = res;
      res->prev = tk;
      tk = res;
    }
  }
  tk->str = ptr;
  tk->size = size;
  tk->next = NULL;
  tk->type = create_tok_type(tk);
  return 1;
}
/**
 ** \fn void  print_token(struct token *tk)
 ** \brief create a token
 **
 ** \return allocate a token and fill it
 **
 */
void print_token(struct token *tk)
{
  struct token *tmp = tk;
  unsigned i = 0;
  while (tmp)
  {
    printf("tk n %u type %d |%s|\n", i, tmp->type, tmp->str);
    i++;
    tmp = tmp->next;
  }
}
/**
 ** \fn char op_tokeniser(char *str, unsigned beg, unsigned end, struct token 
 ** *tk)
 ** \brief create token based on rule
 **
 ** \return if token creation worked 1, 0 otherwise
 **
 */
char op_tokeniser(char *str, unsigned beg, unsigned end, 
    struct set_struct *ss)
{
  const char *c_op[] = {"function", "<<-", "&&", "||", ";;", "<<", ">>", "<&", 
    ">&", "<>", "$(", ";", "|", "&", "!", "(", ")", "'","\n", "\"", "{", "}", 
    ">", "<"};
  unsigned start = beg;
  for (unsigned i = beg; i <= end; i++)
      for (unsigned k = 0; k < sizeof (c_op) / sizeof(*c_op); k++)
      {
        unsigned size = strlen(c_op[k]);
        if ((end - (beg + i) >=  size) && strncmp(c_op[k], str + i , size) == 0)
        {
          if (start < i)
            if (!create_tok(ss, str + start, i - start))
              return 0;
          if (!create_tok(ss, str + i, size))
            return 0;
          i += size;
          start = i;
        }
    }
  if (beg == end)
    return 0;
  if (start < end)
    create_tok(ss, str + start, end-start);
  return 1;
}
