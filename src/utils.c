/**
 * \file utils.c
 * \brief utilities for main program
 * \author Yohann BELAIR
 * \version 0.1
 * \date 13 november 2016
 *
 * Some function to not surcharge main file
 *
 */
#include <stdlib.h>
#include <libgen.h>
#include <stdio.h>
#include <42sh.h>

/**
 * \fn void get_all_env(struct set_struct *ss)
 * \brief get all necesary environement variable and put them inside a struct
 * \return nothing
 *
 */
void get_all_env(struct set_struct *ss)
{
  ss->env = getenv("ENV");
  ss->ifs = getenv("IFS");
  ss->lang = getenv("LANG");
  ss->lc_all = getenv("LC_ALL");
  ss->lc_collate = getenv("LC_COLLATE");
  ss->lc_ctype = getenv("LC_CTYPE");
  ss->lc_messages = getenv("LC_MESSAGES");
  ss->lineno = getenv("LINENO");
  ss->nlspath = getenv("NLSPATH");
  ss->path = getenv("PATH");
  ss->ppid = getenv("PPID");
  ss->ps1 = "42sh$ ";
  ss->ps2 = "> ";
  ss->ps4 = getenv("PS4");
  ss->pwd = getenv("PWD");
  ss->oldpwd = getenv("OLDPWD");
  ss->ast_print = 0;
  ss->home = getenv("HOME");
}
