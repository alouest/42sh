/**
 ** \file ast_printer.c
 ** \brief prints the ast
 ** \author Yohann BELAIR
 ** \version 1.0
 ** \date 27 november 2016
 */
#define _XOPEN_SOURCE 200809L
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <42sh.h>
#include <fcntl.h>
#include <unistd.h>

static void ast_printer_rec(struct ast *a, FILE *fd, int *nb)
{
  int count = *nb;
  if (a->number >= 0)
    for (int i = 0; i <= a->number; i++)
    {
      *nb = (*nb) + 1;
      if (!strcmp(a->child[i]->name, "\n"))
        fprintf(fd, "   %d [label=\"%s\"]\n", *nb, "NEWLINE");
      else
        fprintf(fd, "   %d [label=\"%s\"]\n", *nb, a->child[i]->name);
      fprintf(fd, "   %d -> %d;\n", count, *nb);
      ast_printer_rec(a->child[i], fd, nb);
    }
}

/**
** \fn void ast_printer(struct ast *a)
** \brief prints the AST in dot form is a dot file named dot-ast
** \return nothing
*/

void ast_printer(struct ast *a)
{
  int nb = 0;
  FILE *fl = fopen("dot-ast", "w+");
  if (!fl || !a)
  {
    fprintf(stderr, "AST_PRINTER FAIL\n");
    return ;
  }
  //should check if we are really writting something
  fprintf(fl, "digraph best_graph_ever {\n");
  fprintf(fl, "   %d [label=\"%s\"]\n", nb, a->name);
  ast_printer_rec(a, fl, &nb);
  fprintf(fl, "}");
  fclose(fl);
}
