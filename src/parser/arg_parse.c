/**
** \file arg_parse.c
** \brief checking the args 42sh was called with and changes its behavior
** \version 1.0
** \date 26 november 2016
*/


#define _XOPEN_SOURCE
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <signal.h>
#include <42sh.h>
#include <exec.h>
char help[] = "42sh help:\n-c <command> : \n\
  read commands from the first non-option argument\n\
[-+]O:\n\
  -O set shopt option, +O unset shopt option\n\
--norc:\n\
  desactivate ressource reader\n";

static int norc()
{
  printf("Norc not yet implemented\n");
  return 1;
}
static int ast_print()
{
  struct set_struct *ss = malloc(sizeof (struct set_struct));
  ss->tk = NULL;
  get_all_env(ss);
  printf("42sh $");
  unsigned size;
  char *str = get_stdin(&size);
  proper_tokenise(ss, str, size);
  struct token *tmp_tk = ss->tk;
  while (tmp_tk)
  {
    expansion_manager(ss, tmp_tk);
    tmp_tk = tmp_tk->next;
  }
  print_token(ss->tk);
  //remember to free ast
  ast_printer(parser(&ss->tk));
  printf("ast printing done\n");
  //free(str);
  return 0;
}
static int shopt(char c)
{
  if (c == 0)
    printf("Shopt not yet implemented, option detected : +o\n");
  else
    printf("Shopt not yet implemented, option detected : -o\n");
  return 1;
}
static int version()
{
  printf("Version 1.0\n");
  return 1;
}
static int c(int argc, char *argv[])
{
  for (int i = 2; i < argc; i++)
    if (argv[i][0] != '-')
    {
      struct set_struct *ss = malloc(sizeof (struct set_struct));
      if (!ss)
        return 1;
      get_all_env(ss);
      int ret = proper_tokenise(ss, argv[i], strlen(argv[i]));
      struct token *tmp_tk = ss->tk;
      while (tmp_tk)
      {
        expansion_manager(ss, tmp_tk);
        tmp_tk = tmp_tk->next;
      }
      tmp_tk = ss->tk;
      struct ast *a = parser(&ss->tk);
      ret = exec(a);
      destroy_ast(a);
      free_token(tmp_tk);
      free(ss);
      return ret;
    }
  fprintf(stderr, "42sh: -c: option requires an argument\n");
  return 1;
}

/**
** \fn int arg_parse(int argc, char* argv[])
** \brief parses the argument given to 42sh amd modifies its behavior
** \return -1 if args are not correct, else returns the 42sh return value
*/

int arg_parse(int argc, char* argv[])
{
  /* Not a valid size but not an error */
  if (argc <= 1)
    return -1;
  unsigned size = strlen(argv[1]);
  if (size == 0)
    return -1;
  if (size > 2 && !strncmp("--version", argv[1], size))
    return version();
  if (size > 2 && !strncmp("--norc", argv[1], size))
    return norc();
  if (size > 2 && !strncmp("--ast-print", argv[1], size))
    return ast_print();
  if (!strcmp("-c", argv[1]))
    return c(argc, argv);
  if (size > 2 && !strncmp("-o", argv[1], size))
    return shopt(1);
  if (size > 2 && !strncmp("+o", argv[1], size))
    return shopt(0);
  fprintf(stderr, "Invalid argument \"%s\"\n%s", argv[1], help);
  return 1;
}
