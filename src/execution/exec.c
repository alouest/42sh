/**
** \file exec.c 
** \brief Handle execution form ast token
** \author Benoît MORETTE-BOURNY
** \version 0.5
** \date 17 movember 2016
**
*/
#include "exec.h"
#include <stdio.h>

/**
** \fn int init_exec(struct ast *tree)
** \brief initialise the stack
**
** \return 1 on error, 0 otherwise
*/
int init_exec(struct ast *tree)
{
  //  printf("start exec\n");
  int ret = 0;
  environement = malloc(sizeof (struct env));
  environement->var = new();
  environement->pipe[0] = -1;
  environement->pipe[1] = -1;
  if (environement->var == NULL)
    return 1;
  if (tree->name[0] == '\n')
    return 1;
  ret = exec(tree);
  blast(environement->var);
  free(environement);
  return ret;
}

int exec(struct ast *tree)
{
  if (tree == NULL)
    return 1;
  if (strcmp(tree->name, "input") == 0)
    return init_exec(tree->child[0]);
  if (!strcmp(tree->name, "list") || !strcmp(tree->name, "compound_list"))
    return list(tree);
  if (strcmp(tree->name, "and_or") == 0)
    return logic(tree);
  if (strcmp(tree->name, "pipeline") == 0)
    return sh_pipe(tree);
  if (strcmp(tree->name, "compound_list") == 0)
    return list(tree);
  if (strcmp(tree->name, "rule_for") == 0)
    return sh_for(tree);
  if (strcmp(tree->name, "rule_while") == 0)
    return sh_while(tree);
  if (strcmp(tree->name, "rule_until") == 0)
    return sh_until(tree);
  if (strcmp(tree->name, "rule_if") == 0)
    return sh_if(tree);
  if (strcmp(tree->name, "else_clause") == 0)
    return sh_else(tree);
  if (strcmp(tree->name, "do_group") == 0)
    return exec(tree->child[1]);
  if (strcmp(tree->name, "command") == 0)
    return sh_comm(tree);
  return 1;
}
