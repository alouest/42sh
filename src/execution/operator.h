#ifndef OPE
#define OPE

#include <string.h>
#include "42sh.h"
#include "pile.h"
#include "exec.h"

int logic(struct ast *tree);
int list(struct ast *tree);
int sh_pipe(struct ast *tree);

#endif
