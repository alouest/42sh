#ifndef COND
#define COND

#include <string.h>
#include "42sh.h"
#include "exec.h"


int sh_if(struct ast *tree);
int sh_else(struct ast *tree);

#endif
