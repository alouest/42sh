class my_color:
    HEADER = '\033[95m'
    BLUE = '\033[94m'
    GREEN = '\033[92m'
    YELLOW = '\033[93m'
    RED = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'

print my_color.BLUE + "Test suit 42sh" + my_color.ENDC

print my_color.YELLOW + "Test if our 42sh manage globbing the same way bash does" + my_color.ENDC

