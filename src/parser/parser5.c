#define _XOPEN_SOURCE 600
#include <string.h>
#include <sys/types.h>
#include <pwd.h>
#include <stdlib.h>
#include <string.h>
#include <42sh.h>

struct ast *is_case_clause(l_token **token)
{
  struct ast *ast = create_node("case_clause");
  struct ast *temp = NULL;
  l_token *save = *token;
  add_node(ast, is_case_item(token));
  if (!ast->child[ast->number])
    return destroy_ast(ast);
  while ((*token)->type == DSEMI)
  {
    add_son(ast, ";;");
    *token = (*token)->next;
    while ((*token)->type == NEWLINE)
      *token = (*token)->next;
    save = *token;
    if ((temp = (is_case_item(token))))
      add_node(ast, temp);
    else
    {
      *token = save;
      break;
    }
  }
  while ((*token)->type == NEWLINE)
    *token = (*token)->next;
  return ast;
}

static struct ast *is_case_item3(l_token **token, struct ast *ast)
{
  struct ast *temp = NULL;
  struct token *save = *token;
  if ((*token)->type == Rpar)
  {
    add_son(ast, ")");
    *token = (*token)->next;
  }
  else
    return destroy_ast(ast);
  while ((*token)->type == NEWLINE)
    *token = (*token)->next;
  save = *token;
  if ((temp = is_compound_list(token)))
    add_node(ast, temp);
  else
    *token = save;
  return ast;
}

static struct ast *is_case_item2(l_token **token, struct ast *ast)
{
  while ((*token)->type == Pipe)
  {
    add_son(ast, "|");
    *token = (*token)->next;
    if ((*token)->type == WORD)
    {
      add_son(ast, (*token)->str);
      *token = (*token)->next;
    }
    else
      return destroy_ast(ast);
  }
  return is_case_item3(token, ast);
}

struct ast *is_case_item(l_token **token)
{
  struct ast *ast = create_node("case_item");
  if ((*token)->type == Lpar)
  {
    add_son(ast, "(");
    *token = (*token)->next;
  }
  if ((*token)->type == WORD)
  {
    add_son(ast, (*token)->str);
    *token = (*token)->next;
  }
  else
    return destroy_ast(ast);
  return is_case_item2(token, ast);
}
