/**
** \file condition.c 
** \brief Handle the different 42sh condition
** \author Benoît MORETTE-BOURNY
** \version 0.8
** \date 22 november 2016
*/
#include "condition.h"

/**
** \fn int sh_if(struct ast *tree)
** \brief exeute a 42sh if, according to the SCL
**
** \return 1 on error, 0 on success
*/
int sh_if(struct ast *tree)
{
  int ret = 0;
  int cond = 0;
  cond = exec(tree->child[1]);
  if (cond == 0)
    {
      ret = exec(tree->child[3]);
    }
  else
    {
      if (tree->number >= 5)
        {
          ret = exec(tree->child[4]);
        }
    }
  return ret;
}

/**
** \fn int sh_else(struct ast *tree)
** \brief exeute a 42sh else clasue and elif, according to the SCL
**
** \return 1 on error, 0 on success
*/
int sh_else(struct ast *tree)
{
  int ret = 0;
  if (strcmp(tree->child[0]->name, "else") == 0)
    ret = exec(tree->child[1]);
  else if (strcmp(tree->child[0]->name, "elif") == 0)
    {
      if (exec(tree->child[1]) == 0)
        {
          ret = exec(tree->child[3]);
        }
      else if (tree->number >= 4)
        {
          ret = sh_else(tree->child[4]);
        }
    }
  return ret;
}
