import argparse
import subprocess
import os
class my_color:
    BLUE = '\033[94m'
    GREEN = '\033[92m'
    YELLOW = '\033[93m'
    RED = '\033[91m'
    ENDC = '\033[0m'
    BLINK_RED = '\033[31;5m'
    BLINK_GREEN = '\033[32;5m'
print (my_color.BLUE)
print (".______   .______           ___      .__   __.      _______. __    __")
print ("|   _  \  |   _  \         /   \     |  \ |  |     /       ||  |  |  |")
print ("|  |_)  | |  |_)  |       /  ^  \    |   \|  |    |   (----`|  |__|  |")
print ("|   _  <  |      /       /  /_\  \   |  . `  |     \   \    |   __   |")
print ("|  |_)  | |  |\  \----. /  _____  \  |  |\   | .----)   |   |  |  |  |")
print ("|______/  | _| `._____|/__/     \__\ |__| \__| |_______/    |__|  |__|")
print ()
print ("          -- The shell that breaks the git at D0 --")
print (my_color.ENDC)
print (my_color.BLUE)
print ("-----------------------------------------------")
print ("             Test suite 42sh")
print ("-----------------------------------------------")
print (my_color.ENDC)
parser = argparse.ArgumentParser()
parser.add_argument('-l', '--list', help='display the list of test categories',action='store_true')
parser.add_argument('-c', '--category', help='execute the test suite on the categories passed in argument only')
parser.add_argument('-s', '--sanity', help='execute the test suite with sanity checks enabled', action='store_true')
args = parser.parse_args()
if args.list :
    print ("echo")
    print ("globbing")
    print ("pipes")
if args.category :
    if args.category == "echo" :
        subprocess.call(['python', 'echo/echo_test.py'])
    if args.category == "pipes" :
        subprocess.call(['python', 'pipes/pipes_test.py'])
    if args.category == "globbing" :
        subprocess.call(['python', 'globbing/globbing_test.py'])
if args.sanity :
    print ("Sanity test is launched")
    os.system("python echo/echo_test.py -s")
    os.system("python pipes/pipes_test.py -s")
