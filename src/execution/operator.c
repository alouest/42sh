/**
** \file operator.c 
** \brief Handle the 42sh logic operator, the semi-column and the ampersand
** \author Benoît MORETTE-BOURNY
** \version 0.8
** \date 17 november 2016
*/
#define _GNU_SOURCE
#include <sys/wait.h>
#include <unistd.h>
#include <fcntl.h>
#include "operator.h"

/**
** \fn int logic(struct ast *tree)
** \brief exeute a 42sh logic operator
**
** \return 1 on error, 0 on success
*/
int logic(struct ast *tree)
{
  int ret = 0;
  ret = exec(tree->child[0]);
  int num = 1;
  for (; num <= tree->number; num += 2)
      if (strcmp(tree->child[num]->name, "||") == 0 && ret == 0)
      {
        ret = exec(tree->child[num + 1]);
      }
    else if (strcmp(tree->child[num]->name, "&&") == 0 && ret == 1)
      {
        ret = exec(tree->child[num + 1]);
      }
  return ret;
}

/**
** \fn int list(struct ast *tree)
** \brief exeute a 42sh semi-column and the ampersand symbol
**
** \return 1 on error, 0 on success
*/
int list(struct ast *tree)
{
  int ret = 0;
  ret = exec(tree->child[0]);
  int num = 1;
  for (; num + 1 <= tree->number; num += 2)
      if (strcmp(tree->child[num]->name, "&") == 0)
        {
          ret = exec(tree->child[num + 1]);
        }
      else if (strcmp(tree->child[num]->name, ";") == 0)
        {
          ret = exec(tree->child[num + 1]);
        }
  return ret;
}

void dupl(int in, int out)
{
  if (in != 0)
    {
      dup2(in, 0);
      close(in);
    }
  if (out != 1)
    {
      dup2(out, 1);
      close(out);
    }
}

/**
** \fn int sh_pipe(struct ast *tree)
** \brief exeute a 42sh pipe symbol
**
** \return 1 on error, 0 on success
*/
#include <stdio.h>
int sh_pipe(struct ast *tree)
{
  int ret = 0;
  int in = 0;
  int out = 1;
  int pipe_tab[2];
  int fd0 = dup(0);
  if (tree->number < 1)
    return exec(tree->child[0]);
  for (int i = 0; i < tree->number + 2; i += 2)
    {
      pipe2(pipe_tab, O_CLOEXEC);
      out = pipe_tab[1];
      if (i == tree->number)
        out = 1;
      pid_t pid = fork();
      if (pid == 0)
        {
          dupl(in, out);
          ret = exec(tree->child[i]);
          exit(ret);
        }
      close (pipe_tab[1]);
      in = pipe_tab[0];
    }
  for(int i = 0; i < tree->number / 2 + 1; i++)
    wait(&ret);
  dup2(fd0, 0);
  return !WIFEXITED(ret);
}
