/**
** \file loop.c 
** \brief Handle the different 42sh loops
** \author Benoît MORETTE-BOURNY
** \version 0.8
** \date 15 november 2016
*/

#include "loop.h"

extern struct pile *var;

/**
** \fn int sh_while(struct ast *tree)
** \brief exeute a 42sh while, according to the SCL
**
** \return 1 on error, 0 on success
*/
int sh_while(struct ast *tree)
{
  struct ast *action = tree->child[2]->child[1];
  struct ast *condition = tree->child[1];
  int ret = 0;
  while (exec(condition) == 0)
    ret = exec(action);
  return ret;
}


/**
** \fn int sh_until(struct ast *tree)
** \brief exeute a 42sh util, according to the SCL
**
** \return 1 on error, 0 on success
*/
int sh_until(struct ast *tree)
{
  struct ast *action = tree->child[2]->child[1];
  struct ast *condition = tree->child[1];
  int ret = 0;
  while (exec(condition) != 0)
    ret = exec(action);
  return ret;
}

/**
** \fn int sh_for(struct ast *tree)
** \brief exeute a 42sh for, according to the SCL
**
** \return 1 on error, 0 on success
*/
int sh_for(struct ast *tree)
{
  int ret = 0;
  char *for_var = tree->child[1]->name;
  struct pile *temp = push(environement->var, for_var, 0);
  if (temp == NULL)
    return 1;
  else
    environement->var = temp;
  int size = tree->number - 1;
  for (int it = 3; it <= tree->number &&
         strcmp(tree->child[it]->name, "do_group") != 0; it ++)
    {
      environement->var->value = tree->child[it]->name;
      ret = exec(tree->child[size + 1]);
    }
  pop(environement->var);
  return ret;
}
