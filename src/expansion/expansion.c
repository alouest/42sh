#include <string.h>
#include <42sh.h>
#include <stdio.h>
#include <stdlib.h>

void expansion_manager(struct set_struct *ss, struct token *tk)
{
  if (tk->str[0] == '\'')
  {
    tk->str++;
    unsigned len = strlen(tk->str);
    tk->str[len - 1] = 0;
    return ;
  }
  rnd_expansion(tk);
  rnd_expansion2(tk);
  if (tk->str[0] == '"')
  {
    tk->str++;
    unsigned len = strlen(tk->str);
    tk->str[len - 1] = 0;
    return ;
  }
  tilde_expansion(ss, tk);
  tilde_plus_expansion(ss, tk);
  tilde_minus_expansion(ss, tk);
  rnd_expansion(tk);
  rnd_expansion2(tk);
}
