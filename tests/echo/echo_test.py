import subprocess
import argparse
class my_color:
    BLUE = '\033[94m'
    GREEN = '\033[92m'
    YELLOW = '\033[93m'
    RED = '\033[91m'
    ENDC = '\033[0m'
    BLINK_RED = '\033[31;5m'
    BLINK_GREEN = '\033[32;5m'
final = 0
parser = argparse.ArgumentParser()
parser.add_argument('-s', '--sanity', help='execute the test suite with sanity checks enabled', action='store_true')
args = parser.parse_args()
if args.sanity :
    print (my_color.BLUE + "Test suit 42sh with sanity check" + my_color.ENDC)
    print (my_color.YELLOW + "Test if echo in 42sh work like echo in bash" + my_color.ENDC)
    print ("-------------------------------------------")
    print (my_color.BLUE + "         Testing echo poney" + my_color.ENDC)
    ret = subprocess.run(["valgrind --leak-check=full --error-exitcode=42 ./../42sh -c \"echo poney\""], shell=True, stdout=subprocess.PIPE)
    if ret.returncode == 42:
        final = final + 1
        print (my_color.BLINK_RED)
        print ("-------------------------------------------")
        print ("                   FAIL") 
        print ("-------------------------------------------")
        print (my_color.ENDC)
    else:
        print (my_color.BLINK_GREEN)
        print ("-------------------------------------------")
        print ("                   OK") 
        print ("-------------------------------------------")
        print (my_color.ENDC)

    print (my_color.BLUE + "         Testing echo ~" + my_color.ENDC)
    ret = subprocess.run(["valgrind --leak-check=full --error-exitcode=42 ./../42sh -c \"echo ~\""], shell=True, stdout=subprocess.PIPE)
    if ret.returncode == 42:
        final = final + 1
        print (my_color.BLINK_RED)
        print ("-------------------------------------------")
        print ("                   FAIL") 
        print ("-------------------------------------------")
        print (my_color.ENDC)
    else:
        print (my_color.BLINK_GREEN)
        print ("-------------------------------------------")
        print ("                   OK") 
        print ("-------------------------------------------")
        print (my_color.ENDC)


    print (my_color.BLUE + "         Testing echo ~-" + my_color.ENDC)
    ret = subprocess.run(["valgrind --leak-check=full --error-exitcode=42 ./../42sh -c \"echo ~-\""], shell=True, stdout=subprocess.PIPE)
    if ret.returncode == 42:
        final = final + 1
        print (my_color.BLINK_RED)
        print ("-------------------------------------------")
        print ("                   FAIL") 
        print ("-------------------------------------------")
        print (my_color.ENDC)
    else:
        print (my_color.BLINK_GREEN)
        print ("-------------------------------------------")
        print ("                   OK") 
        print ("-------------------------------------------")
        print (my_color.ENDC)
        print ("-------------------------------------------")


    print (my_color.BLUE + "         Testing echo ~+" + my_color.ENDC)
    ret = subprocess.run(["valgrind --leak-check=full --error-exitcode=42 ./../42sh -c \"echo ~+\""], shell=True, stdout=subprocess.PIPE)
    if ret.returncode == 42:
        final = final + 1
        print (my_color.BLINK_RED)
        print ("-------------------------------------------")
        print ("                   FAIL") 
        print ("-------------------------------------------")
        print (my_color.ENDC)
    else:
        print (my_color.BLINK_GREEN)
        print ("-------------------------------------------")
        print ("                   OK") 
        print ("-------------------------------------------")
        print (my_color.ENDC)
        print ("-------------------------------------------")


    print (my_color.BLUE + "         Testing echo ~+/poney" + my_color.ENDC)
    ret = subprocess.run(["valgrind --leak-check=full --error-exitcode=42 ./../42sh -c \"echo ~+/poney\""], shell=True, stdout=subprocess.PIPE)
    if ret.returncode == 42:
        final = final + 1
        print (my_color.BLINK_RED)
        print ("-------------------------------------------")
        print ("                   FAIL") 
        print ("-------------------------------------------")
        print (my_color.ENDC)
    else:
        print (my_color.BLINK_GREEN)
        print ("-------------------------------------------")
        print ("                   OK") 
        print ("-------------------------------------------")
        print (my_color.ENDC)
        print ("-------------------------------------------")

    print (my_color.BLUE + "         Testing echo ~+/poney" + my_color.ENDC)
    ret = subprocess.run(["valgrind --leak-check=full --error-exitcode=42 ./../42sh -c \"echo ~+/poney\""], shell=True, stdout=subprocess.PIPE)
    if ret.returncode == 42:
        final = final + 1
        print (my_color.BLINK_RED)
        print ("-------------------------------------------")
        print ("                   FAIL") 
        print ("-------------------------------------------")
        print (my_color.ENDC)
    else:
        print (my_color.BLINK_GREEN)
        print ("-------------------------------------------")
        print ("                   OK") 
        print ("-------------------------------------------")
        print (my_color.ENDC)
        print ("-------------------------------------------")

    print (my_color.BLUE + "         Testing echo ~+/poney" + my_color.ENDC)
    ret = subprocess.run(["valgrind --leak-check=full --error-exitcode=42 ./../42sh -c \"echo ~+/poney\""], shell=True, stdout=subprocess.PIPE)
    if ret.returncode == 42:
        final = final + 1
        print (my_color.BLINK_RED)
        print ("-------------------------------------------")
        print ("                   FAIL") 
        print ("-------------------------------------------")
        print (my_color.ENDC)
    else:
        print (my_color.BLINK_GREEN)
        print ("-------------------------------------------")
        print ("                   OK") 
        print ("-------------------------------------------")
        print (my_color.ENDC)
        print ("-------------------------------------------")
    print ("              Test result")
    if final > 0 :
        print (my_color.BLINK_RED + "                    KO" + my_color.ENDC)
    else :
        print (my_color.BLINK_GREEN + "                    OK" + my_color.ENDC)
else :
    print (my_color.BLUE + "Test suit 42sh" + my_color.ENDC)
    print (my_color.YELLOW + "Test if echo in 42sh work like echo in bash" + my_color.ENDC)
    print ("-------------------------------------------")
    print (my_color.BLUE + "         Testing echo poney" + my_color.ENDC)
    ret = subprocess.run(["./../42sh -c \"echo poney\""], shell=True, stdout=subprocess.PIPE)
    bret = subprocess.run(["echo poney"], shell=True, stdout=subprocess.PIPE)
    if ret.stdout != bret.stdout and ret.returncode != bret.returncode:
        print (my_color.BLINK_RED + "         KO" + my_color.ENDC)
        final = final + 1
    else:
        print (my_color.BLINK_GREEN + "         OK" + my_color.ENDC)
    print ("-------------------------------------------")

    print ("-------------------------------------------")
    print (my_color.BLUE + "         Testing echo ~" + my_color.ENDC)
    ret = subprocess.run(["./../42sh -c \"echo ~\""], shell=True, stdout=subprocess.PIPE)
    bret = subprocess.run(["echo ~"], shell=True, stdout=subprocess.PIPE)
    if ret.stdout != bret.stdout and ret.returncode != bret.returncode:
        print (my_color.BLINK_RED + "         KO" + my_color.ENDC)
        final = final + 1
    else:
        print (my_color.BLINK_GREEN + "         OK" + my_color.ENDC)
    print ("-------------------------------------------")

    print ("-------------------------------------------")
    print (my_color.BLUE + "         Testing echo ~-" + my_color.ENDC)
    ret = subprocess.run(["./../42sh -c \"echo ~-\""], shell=True, stdout=subprocess.PIPE)
    bret = subprocess.run(["echo ~-"], shell=True, stdout=subprocess.PIPE)
    if ret.stdout != bret.stdout and ret.returncode != bret.returncode:
        print (my_color.BLINK_RED + "         KO" + my_color.ENDC)
        final = final + 1
    else:
        print (my_color.BLINK_GREEN + "         OK" + my_color.ENDC)
    print ("-------------------------------------------")


    print ("-------------------------------------------")
    print (my_color.BLUE + "         Testing echo ~+" + my_color.ENDC)
    ret = subprocess.run(["./../42sh -c \"echo ~+\""], shell=True, stdout=subprocess.PIPE)
    bret = subprocess.run(["echo ~+"], shell=True, stdout=subprocess.PIPE)
    if ret.stdout != bret.stdout and ret.returncode != bret.returncode:
        print (my_color.BLINK_RED + "         KO" + my_color.ENDC)
        final = final + 1
    else:
        print (my_color.BLINK_GREEN + "         OK" + my_color.ENDC)
    print ("-------------------------------------------")


    print ("-------------------------------------------")
    print (my_color.BLUE + "         Testing echo ~/poney" + my_color.ENDC)
    ret = subprocess.run(["./../42sh -c \"echo ~/poney\""], shell=True, stdout=subprocess.PIPE)
    bret = subprocess.run(["echo ~/poney"], shell=True, stdout=subprocess.PIPE)
    if ret.stdout != bret.stdout and ret.returncode != bret.returncode:
        print (my_color.BLINK_RED + "         KO" + my_color.ENDC)
        final = final + 1
    else:
        print (my_color.BLINK_GREEN + "         OK" + my_color.ENDC)
    print ("-------------------------------------------")

    print ("-------------------------------------------")
    print (my_color.BLUE + "         Testing echo ~-/poney" + my_color.ENDC)
    ret = subprocess.run(["./../42sh -c \"echo ~-/poney\""], shell=True, stdout=subprocess.PIPE)
    bret = subprocess.run(["echo ~-/poney"], shell=True, stdout=subprocess.PIPE)
    if ret.stdout != bret.stdout and ret.returncode != bret.returncode:
        print (my_color.BLINK_RED + "         KO" + my_color.ENDC)
        final = final + 1
    else:
        print (my_color.BLINK_GREEN + "         OK" + my_color.ENDC)
    print ("-------------------------------------------")


    print ("-------------------------------------------")
    print (my_color.BLUE + "         Testing echo ~+/poney" + my_color.ENDC)
    ret = subprocess.run(["./../42sh -c \"echo ~+/poney\""], shell=True, stdout=subprocess.PIPE)
    bret = subprocess.run(["echo ~+/poney"], shell=True, stdout=subprocess.PIPE)
    if ret.stdout != bret.stdout and ret.returncode != bret.returncode:
        print (my_color.BLINK_RED + "         KO" + my_color.ENDC)
        final = final + 1
    else:
        print (my_color.BLINK_GREEN + "         OK" + my_color.ENDC)
    print ("-------------------------------------------")

    print ("              Test result")
    if final > 0:
        print (my_color.BLINK_RED + "                    KO" + my_color.ENDC)
    else:
        print (my_color.BLINK_GREEN + "                    OK" + my_color.ENDC)
    
