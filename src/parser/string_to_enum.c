/**
** \file string_to_enum.c
** \brief converts a token_type enum to a malloc'ed string
** \author Tristan RUTER-NAON
** \version 1.0
** \date 27 november 2016
*/

#define _XOPEN_SOURCE 600
#include <42sh.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>

static char *to_string4(enum token_type type)
{
  char *str = NULL;
  if (type == Bang)
    str = strdup("!");
  else if (type == In)
    str = strdup("in");
  else if (type == NEWLINE)
    str = strdup("\n");
  else if(type == WORD)
    str = strdup("word");
  else if(type == ASSIGNEMENT_WORD)
    str = strdup("assignement_word");
  else if(type == NAME)
    str = strdup("name");
  else if (type == IO_NUMBER)
    str = strdup("io_number");
  else if (type == HEREDOC)
    str = strdup("heredoc");
  return str;
}
static char *to_string3(enum token_type type)
{
  char *str = NULL;
  if (type == Fi)
    str = strdup("fi");
  else if (type == Do)
    str = strdup("do");
  else if (type == Done)
    str = strdup("done");
  else if (type == Case)
    str = strdup("case");
  else if (type == Esac)
    str = strdup("esac");
  else if (type == While)
    str = strdup("while");
  else if (type == Until)
    str = strdup("until");
  else if (type == For)
    str = strdup("for");
  else if (type == Lbrace)
    str = strdup("{");
  else if (type == Rbrace)
    str = strdup("}");
  return str ? str : to_string4(type);
}
static char *to_string2(enum token_type type)
{
  char *str = NULL;
  if (type == DLESS)
    str = strdup("<<");
  else if (type == DGREAT)
    str = strdup(">>");
  else if (type == LESSAND)
    str = strdup("<&");
  else if (type == GREATAND)
    str = strdup(">&");
  else if (type == LESSGREAT)
    str = strdup("<>");
  else if (type == DLESSDASH)
    str = strdup("<<-");
  else if (type == CLOBBER)
    str = strdup(">|");
  else if (type == If)
    str = strdup("if");
  else if (type == Then)
    str = strdup("then");
  else if (type == Else)
    str = strdup("else");
  else if (type == Elif)
    str = strdup("elif");
  return str ? str : to_string3(type);
}

/**
** \fn char *to_string(enum token_type type)
** \brief finds the corresponding string for a token_type and returns it
** \return malloc'ed string with the corresponding type
*/

char *to_string(enum token_type type)
{
  char *str = NULL;
  if (type == AND_IF)
    str = strdup("&&");
  else if (type == COMMA)
    str = strdup(";");
  else if (type == Lpar)
    str = strdup("(");
  else if (type == Rpar)
    str = strdup(")");
  else if (type == Pipe)
    str = strdup("|");
  else if (type == AND)
    str = strdup("&");
  else if (type == OR_IF)
    str = strdup("||");
  else if (type == DSEMI)
    str = strdup(";;");
  return str ? str : to_string2(type);
}

