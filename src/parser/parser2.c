/**
** \file parser2.c
** \brief tools for parsing an LL language from the subject
** (more or less a bash grammar)
** \version 1.0
** \date 27 november 2016
*/

#define _XOPEN_SOURCE 600
#include <string.h>
#include <sys/types.h>
#include <pwd.h>
#include <stdlib.h>
#include <string.h>
#include <42sh.h>

/**
** \fn struct ast *is_simple_command(l_token **token)
** \brief parses the pointer to list of token to check if it matched
** the simple_command and returns a malloc'ed AST
** \return NULL if rule is not verified, else returns the corresponding AST
*/

struct ast *is_simple_command(l_token **token)
{
  struct token *save = *token;
  struct ast *ast = create_node("simple_command");
  struct ast *temp = NULL;
  while ((temp = is_prefix(token)))
  {
    save = *token;
    add_node(ast, temp);
  }
  if (ast->number < 0)
    *token = save;
  while((temp = is_element(token)))
    add_node(ast, temp);
  if (ast->number < 0)
    return destroy_ast(ast);
  return ast;
}

static struct ast *which_rule(l_token **token, struct ast *ast)
{
  struct ast *temp = NULL;
  l_token *save = *token;
  if ((temp = is_rule_for(token)))
    return add_node(ast, temp);
  *token = save;
  if ((temp = is_rule_while(token)))
    return add_node(ast, temp);
  *token = save;
  if ((temp = is_rule_until(token)))
    return add_node(ast, temp);
  *token = save;
  if ((temp = is_rule_case(token)))
    return add_node(ast, temp);
  *token = save;
  if ((temp = is_rule_if(token)))
    return add_node(ast, temp);
  return NULL;
}

/**
** \fn struct ast *is_shell_command(l_token **token)
** \brief parses the pointer to list of token to check if it matched
** the shell_command and returns a malloc'ed AST
** \return NULL if rule is not verified, else returns the corresponding AST
*/

struct ast *is_shell_command(l_token **token)
{
  struct ast *temp = NULL;
  struct ast *ast = create_node("shell_command");
  if ((*token)->type == Lpar || (*token)->type == Lbrace)
    add_son(ast, to_string((*token)->type));
  *token = ((*token)->type == Lpar || (*token)->type == Lbrace)
           ? (*token)->next : *token;
  if (ast->number >= 0)
  {
    if ((temp = is_compound_list(token)))
    {
      add_node(ast, temp);
      if ((*token)->type == Rpar || (*token)->type == Rbrace)
      {
        add_son(ast, to_string((*token)->type));
        *token = (*token)->next;
        return ast;
      }
    }
    return destroy_ast(ast);
  }
  return which_rule(token, ast);
}

/**
** \fn struct ast *is_funcdec(l_token **token)
** \brief parses the pointer to list of token to check if it matched
** the funcdec and returns a malloc'ed AST
** \return NULL if rule is not verified, else returns the corresponding AST
*/

struct ast *is_funcdec(l_token **token)
{
  struct ast *ast = create_node("fundec");
  if (((*token)->type == FUNCTION))
  {
    add_son(ast, "function");
    *token = (*token)->next;
  }
  if ((*token)->type != WORD)
    return destroy_ast(ast);
  add_son(ast, "word");
  *token = (*token)->next;
  if ((*token)->type != Lpar)
    return destroy_ast(ast);
  add_son(ast, "(");
  *token = (*token)->next;
  if ((*token)->type != Rpar)
    return destroy_ast(ast);
  add_son(ast, ")");
  *token = (*token)->next;
  while ((*token)->type == NEWLINE)
    *token = (*token)->next;
  add_node(ast, is_shell_command(token));
  if (!ast->child[ast->number])
    return destroy_ast(ast);
  return ast;
}

static struct ast *is_redirection2(l_token **token, struct ast *ast)
{
  if ((*token)->type == DLESS || (*token)->type == DLESSDASH)
  {
    add_son(ast, to_string((*token)->type));
    *token = (*token)->next;
    if ((*token)->type == HEREDOC)
    {
      add_son(ast, (*token)->str);
      *token = (*token)->next;
      return ast;
    }
    else
      return destroy_ast(ast);
  }
  return destroy_ast(ast);
}

/**
** \fn struct ast *is_redirection(l_token **token)
** \brief parses the pointer to list of token to check if it matched
** the redirection and returns a malloc'ed AST
** \return NULL if rule is not verified, else returns the corresponding AST
*/

struct ast *is_redirection(l_token **token)
{
  if (*token == NULL)
    return NULL;
  struct ast *ast = create_node("redirection");
  if ((*token)->type != IO_NUMBER)
    return destroy_ast(ast);
  add_son(ast, "ionumber");
  *token = (*token)->next;
  if ((*token)->type == GREAT || (*token)->type == LESS || (*token)->type
       == DGREAT || (*token)->type == LESSAND || (*token)->type == GREATAND
      || (*token)->type == LESSGREAT || (*token)->type == CLOBBER)
  {
    add_son(ast, to_string((*token)->type));
    *token = (*token)->next;
    if ((*token)->type == WORD)
    {
      add_son(ast, (*token)->str);
      *token = (*token)->next;
      return ast;
    }
    else
      return destroy_ast(ast);
  }
  return is_redirection2(token, ast);
}
