/**
** \file parser3.c
** \brief tools for parsing an LL language from the subject
** (more or less a bash grammar)
** \author Tristan RUTER-NAON
** \version 1.0
** \date 27 november 2016
*/

#define _XOPEN_SOURCE 600
#include <string.h>
#include <sys/types.h>
#include <pwd.h>
#include <stdlib.h>
#include <string.h>
#include <42sh.h>

/**
** \fn struct ast *is_function(l_token **token)
** \brief parses the pointer to list of token to check if it matched the
** prefix and returns a malloc'ed AST
** \return NULL if rule is not verified, else returns the corresponding AST
*/

struct ast *is_prefix(l_token **token)
{
  struct ast *ast = create_node("prefix");
  struct ast *temp = NULL;
  if ((*token)->type == ASSIGNEMENT_WORD)
  {
    add_son(ast, (*token)->str);
    *token = (*token)->next;
    return ast;
  }
  else if ((temp = is_redirection(token)))
    return add_node(ast, temp);
  return destroy_ast(ast);
}

/**
** \fn struct ast *is_element(l_token **token)
** \brief parses the pointer to list of token to check if it matched the
** element and returns a malloc'ed AST
** \return NULL if rule is not verified, else returns the corresponding AST
*/

struct ast *is_element(l_token **token)
{
  if (*token == NULL)
    return NULL;
  struct ast *ast = create_node("element");
  struct ast *temp = NULL;
  if ((*token)->type == WORD)
  {
    add_son(ast, (*token)->str);
    *token = (*token)->next;
    return ast;
  }
  else if ((temp = is_redirection(token)))
    return add_node(ast, temp);
  return destroy_ast(ast);
}

/**
** \fn struct ast *is_compound_list(l_token **token)
** \brief parses the pointer to list of token to check if it matched the
** compound_list and returns a malloc'ed AST
** \return NULL if rule is not verified, else returns the corresponding AST
*/

struct ast *is_compound_list(l_token **token)
{
  struct token *save = *token;
  struct ast *ast = create_node("compound_list");
  struct ast *temp = NULL;
  while ((*token)->type == NEWLINE)
    *token = (*token)->next;
  if (!(temp = is_and_or(token)))
    return destroy_ast(ast);
  add_node(ast, temp);
  while ((*token)->type == COMMA || (*token)->type == AND ||
          (*token)->type == NEWLINE)
  {
    add_son(ast, to_string((*token)->type));
    *token = (*token)->next;
    while (*token != NULL && (*token)->type == NEWLINE)
      *token = (*token)->next;
    save = *token;
    if (*token == NULL)
      return ast;
    ((temp = is_and_or(token)) != NULL) ? add_node(ast, temp) : ast;
    if (temp == NULL)
    {
      *token = save;
      break;
    }
  }
  return ast;
}

static struct ast *is_rule_for2(l_token **token, struct ast *ast, int comma)
{
  struct ast *temp = NULL;
  while ((*token)->type == NEWLINE)
    *token = (*token)->next;
  if ((*token)->type == In && !comma)
  {
    add_son(ast, "in");
    *token = (*token)->next;
    while ((*token)->type == WORD)
    {
      add_son(ast, (*token)->str);
      *token = (*token)->next;
    }
    if ((*token)->type == COMMA || (*token)->type == NEWLINE)
    {
      add_son(ast, to_string((*token)->type));
      *token = (*token)->next;
    }
    else
      return destroy_ast(ast);
  }
  if ((temp = is_do_group(token)))
    return add_node(ast, temp);
  return destroy_ast(ast);
}

/**
** \fn struct ast *is_rule_for(l_token **token)
** \brief parses the pointer to list of token to check if it matched the
** rule_for and returns a malloc'ed AST
** \return NULL if rule is not verified, else returns the corresponding AST
*/

struct ast *is_rule_for(l_token **token)
{
  struct ast *ast = create_node("rule_for");
  int comma = 0;
  if ((*token)->type == For)
  {
    add_son(ast, "for");
    *token = (*token)->next;
  }
  else
    return destroy_ast(ast);
  if ((*token)->type == WORD)
  {
    add_son(ast, (*token)->str);
    *token = (*token)->next;
  }
  else
    return destroy_ast(ast);
  if ((*token)->type == COMMA)
  {
    add_son(ast, ";");
    *token = (*token)->next;
    comma = 1;
  }
  return is_rule_for2(token, ast, comma);
}

/**
** \fn struct ast *is_rule_while(l_token **token)
** \brief parses the pointer to list of token to check if it matched the
** rule_while and returns a malloc'ed AST
** \return NULL if rule is not verified, else returns the corresponding AST
*/

struct ast *is_rule_while(l_token **token)
{
  struct ast *ast = create_node("rule_while");
  struct ast *temp = NULL;
  if ((*token)->type == While)
  {
    add_son(ast, "while");
    *token = (*token)->next;
  }
  else
    return destroy_ast(ast);
  if ((temp = is_compound_list(token)))
    add_node(ast, temp);
  else
    return destroy_ast(ast);
  if ((temp = is_do_group(token)))
    add_node(ast, temp);
  else
    return destroy_ast(ast);
  return ast;
}
