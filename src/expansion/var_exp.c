#define _XOPEN_SOURCE 600
#include <string.h>
#include <42sh.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

static int contain_rnd(struct token *tk)
{
  for (unsigned i = 0; tk->str[i] != 0; i++)
    if (strncmp(tk->str + i, "$RANDOM", 7) == 0)
      return i;
  return -42;
}

static int contain_rnd2(struct token *tk)
{
  for (unsigned i = 0; tk->str[i] != 0; i++)
    if (strncmp(tk->str + i, "${RANDOM}", 9) == 0)
      return i;
  return -42;
}

void rnd_expansion(struct token *tk)
{
  int pos = contain_rnd(tk);
  if (tk->type == WORD && pos >= 0)
  {
    srand(time(NULL));
    int rnd = rand();
    int cpy = rnd;
    unsigned nb_char = 0;
    while (cpy)
    {
      nb_char++;
      cpy /= 10;
    }
    tk->str = realloc(tk->str, sizeof (char) * (strlen(tk->str) 
          + nb_char));
    char *temp_val = malloc(sizeof (char) * strlen(tk->str));
    temp_val = strcpy(temp_val, tk->str);
    temp_val += pos + 7;
    char *temp_val2 = malloc(sizeof (char) * strlen(tk->str));
    temp_val2 = strcpy(temp_val2, tk->str);
    temp_val2[pos] = 0;
    sprintf(tk->str, "%s%d%s", temp_val2, rnd, temp_val);
  }
}
void rnd_expansion2(struct token *tk)
{
  int pos = contain_rnd2(tk);
  if (tk->type == WORD && pos >= 0)
  {
    srand(time(NULL));
    int rnd = rand();
    int cpy = rnd;
    unsigned nb_char = 0;
    while (cpy)
    {
      nb_char++;
      cpy /= 10;
    }
    tk->str = realloc(tk->str, sizeof (char) * (strlen(tk->str) 
          + nb_char));
    char *temp_val = malloc(sizeof (char) * strlen(tk->str));
    temp_val = strcpy(temp_val, tk->str);
    temp_val += pos + 9;
    char *temp_val2 = malloc(sizeof (char) * strlen(tk->str));
    temp_val2 = strcpy(temp_val2, tk->str);
    temp_val2[pos] = 0;
    sprintf(tk->str, "%s%d%s", temp_val2, rnd, temp_val);
  }
}
