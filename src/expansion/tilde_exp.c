#include <string.h>
#include <42sh.h>
#include <stdio.h>
#include <stdlib.h>
static int contain_tilde(struct token *tk)
{
  for (unsigned i = 0; tk->str[i] != 0; i++)
    if (tk->str[i] == '=' && tk->str[i + 1] == '~' && !(tk->str[i + 2] == '-' 
            || tk->str[i + 2] == '+'))
      return i + 1;
  return -42;
}

static int contain_tilde_plus(struct token *tk)
{
  for (unsigned i = 0; tk->str[i] != 0; i++)
    if (tk->str[i] == '=' && tk->str[i + 1] == '~' && tk->str[i + 2] == '+')
      return i + 1;
  return -42;
}

static int contain_tilde_minus(struct token *tk)
{
  for (unsigned i = 0; tk->str[i] != 0; i++)
    if (tk->str[i] == '=' && tk->str[i + 1] == '~' && tk->str[i + 2] == '-')
      return i + 1;
  return -42;
}

void tilde_expansion(struct set_struct *ss, struct token *tk)
{
  unsigned nb = 0;
  if (tk->type == WORD && tk->str[0] == '~' && ! (tk->str[1] == '-' 
        || tk->str[1] == '+'))
  {
    nb ++;
    tk->str = realloc(tk->str, sizeof (char) * (strlen(tk->str) 
          + nb * strlen(ss->home)));
    char *temp_val = malloc(sizeof (char) * strlen(tk->str));
    temp_val = strcpy(temp_val, tk->str);
    temp_val++;
    sprintf(tk->str, "%s%s", ss->home, temp_val);
  }
  int pos = contain_tilde(tk);
  if (tk->type == ASSIGNEMENT_WORD && pos > 0)
  {
    nb ++;
    tk->str = realloc(tk->str, sizeof (char) * (strlen(tk->str) 
          + nb * strlen(ss->home)));
    char *temp_val = malloc(sizeof (char) * strlen(tk->str));
    temp_val = strcpy(temp_val, tk->str);
    temp_val += pos + 1;
    char *temp_val2 = malloc(sizeof (char) * strlen(tk->str));
    temp_val2 = strcpy(temp_val2, tk->str);
    temp_val2[pos] = 0;
    sprintf(tk->str, "%s%s%s", temp_val2, ss->home, temp_val);
  }
}

void tilde_plus_expansion(struct set_struct *ss, struct token *tk)
{
  unsigned nb = 0;
  if (tk->type == WORD && tk->str[0] == '~' && tk->str[1] == '+' )
  {
    nb ++;
    tk->str = realloc(tk->str, sizeof (char) * (strlen(tk->str) 
          + nb * strlen(ss->pwd)));
    char *temp_val = malloc(sizeof (char) * strlen(tk->str));
    temp_val = strcpy(temp_val, tk->str);
    temp_val += 2;
    sprintf(tk->str, "%s%s", ss->pwd, temp_val);
  }
  int pos = contain_tilde_plus(tk);
  if (tk->type == ASSIGNEMENT_WORD && pos > 0)
  {
    nb ++;
    tk->str = realloc(tk->str, sizeof (char) * (strlen(tk->str) 
          + nb * strlen(ss->pwd)));
    char *temp_val = malloc(sizeof (char) * strlen(tk->str));
    temp_val = strcpy(temp_val, tk->str);
    temp_val += pos + 2;
    char *temp_val2 = malloc(sizeof (char) * strlen(tk->str));
    temp_val2 = strcpy(temp_val2, tk->str);
    temp_val2[pos] = 0;
    sprintf(tk->str, "%s%s%s", temp_val2, ss->pwd, temp_val);
  }
}

void tilde_minus_expansion(struct set_struct *ss, struct token *tk)
{
  unsigned nb = 0;
  if (tk->type == WORD && tk->str[0] == '~' && tk->str[1] == '-' )
  {
    nb ++;
    tk->str = realloc(tk->str, sizeof (char) * (strlen(tk->str) + nb 
          * strlen(ss->oldpwd)));
    char *temp_val = malloc(sizeof (char) * strlen(tk->str));
    temp_val = strcpy(temp_val, tk->str);
    temp_val += 2;
    sprintf(tk->str, "%s%s", ss->oldpwd, temp_val);
  }
  int pos = contain_tilde_minus(tk);
  if (tk->type == ASSIGNEMENT_WORD && pos > 0)
  {
    nb ++;
    tk->str = realloc(tk->str, sizeof (char) * (strlen(tk->str) + nb 
          * strlen(ss->oldpwd)));
    char *temp_val = malloc(sizeof (char) * strlen(tk->str));
    temp_val = strcpy(temp_val, tk->str);
    temp_val += pos + 2;
    char *temp_val2 = malloc(sizeof (char) * strlen(tk->str));
    temp_val2 = strcpy(temp_val2, tk->str);
    temp_val2[pos] = 0;
    sprintf(tk->str, "%s%s%s", temp_val2, ss->oldpwd, temp_val);
  }
}
